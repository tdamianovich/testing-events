import React from 'react';
import '../App.css';

const Button = (props) => {
  const handleClick = (e) => {
    e.preventDefault();
    props.toggleClick();
  }
  return (
    <button onClick={handleClick}>{props.text}</button>
  )};

export default Button;
