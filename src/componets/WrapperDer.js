import React from 'react';
import '../App.css';

const WrapperDer = (props) => {
  const handleClick = (name,e) => {
    e.preventDefault();
    props.changeText(name);
  }
  var text = (props.text) ? props.text : '';
  if (props.changeText) {
    return (<div className="dottedButton" onMouseEnter={handleClick.bind(this,"Inside!")} onMouseLeave={handleClick.bind(this,"")}>{text}</div>);
  } else {
    return (<div className="dottedButton">{text}</div>);
  }
}
export default WrapperDer;