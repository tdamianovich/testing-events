import React from 'react';
import './App.css';
import Button from './componets/Button';
// import WrapperIzq from './componets/WrapperIzq';
import WrapperDer from './componets/WrapperDer';

class App extends React.Component {
  constructor(props){
    super(props);
    this.state={
      isClicked: false,
      name : ''
    }
  }
  toggleClick = () => {
    this.setState({
      isClicked: !this.state.isClicked
    });
    console.log(`This state is ${this.state.isClicked}`)
  }
  changeText = (newName) => {
    this.setState({
      name: newName
    });
  }
  render() {
  return (
    <div className="App">
      <Button text="Click me!" toggleClick={this.toggleClick}></Button>
      <WrapperDer text="Soy un Div!" changeText={this.changeText}></WrapperDer> 
      <WrapperDer text={this.state.name}></WrapperDer> 
      {/* <WrapperDer></WrapperDer>  */}
    </div>
  )};
}

export default App;
